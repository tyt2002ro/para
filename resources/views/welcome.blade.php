<!DOCTYPE html>
@extends('layouts.app')
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts 
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
-->
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 75vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    @section('content')
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                
May Fly! <a href="http://www.para.gq/gliders">(new version HERE!)</a>

                    May Fly!
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>

                
                    <div id="myCarousel" class="carousel fdi-Carousel slide" title="Data source">
                     <!-- Carousel items -->
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                                <div class="item active">
                                       <div class="col-md-1" >
                                        <a href="http://www.xcontest.org/world/en/"><img src="{{ url('/pictures/xcontest.gif') }}"  class="img-responsive center-block"></a>
                                        <div class="text-center">1</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-1">
                                        <a href="https://www.delta-club-82.com/bible/hang-glider-bible.php"><img src="{{ url('/pictures/logoDC82.png') }}" height="150" width="150" class="img-responsive center-block"></a>
                                        <div class="text-center">2</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-1">
                                        <a href="http://para2000.org/wings/index.html"><img src="{{ url('/pictures/para2000_large_new.gif') }}" height="150" width="150" class="img-responsive center-block"></a>
                                        <div class="text-center">3</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-1">
                                        <a href="http://www.flyhy.co/tools"><img src="{{ url('/pictures/flyhy.png') }}" height="150" width="150" class="img-responsive center-block"></a>
                                        <div class="text-center">4</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-1">
                                        <a href="http://www.leonardoxc.net/see-it-in-action"><img src="{{ url('/pictures/leonardo_logo.gif') }}" height="150" width="150" class="img-responsive center-block"></a>
                                        <div class="text-center">5</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-1">
                                        <a href="http://paraglidinglogbook.com/"><img src="{{ url('/pictures/plog.png') }}" height="150" width="150" class="img-responsive center-block"></a>
                                        <div class="text-center">6</div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="col-md-1">
                                        <a href="https://www.xcleague.com/xc/flights/20163388.html?vx=0"><img src="{{ url('/pictures/xc.png') }}" height="150" width="150" class="img-responsive center-block"></a>
                                        <div class="text-center">5</div>
                                    </div>
                                </div>


                                <div class="item">
                                    <div class="col-md-1">
                                        <a href="http://parapente.ffvl.fr/cfd/liste/2000?sort=asc&order=date"><img src="{{ url('/pictures/ffvl_white_logo.png') }}" height="150" width="150" class="img-responsive center-block"></a>
                                        <div class="text-center">5</div>
                                    </div>
                                </div>


                            </div>
                            <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                            <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                        </div>
                        <!--/carousel-inner-->
                    </div><!--/myCarousel-->
                <!--/well-->



                    <div id="myCarousel1" class="carousel fdi-Carousel slide" title="Media">
                     <!-- Carousel items -->
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                                <div class="item active">
                                       <div class="col-md-1" >
                                        <a href="http://www.paraglidingforum.com/index.php"><img src="{{ url('/pictures/logo2.png') }}" target="_blank" class="img-responsive center-block"></a>
                                        <div class="text-center">Paragliding forum</div>
                                    </div>
                                </div>

                                <div class="item">
                                       <div class="col-md-1" >
                                        <a href="https://www.theparaglider.com/"><img src="{{ url('/pictures/the_paraglider_logo_small.jpg') }}" target="_blank" class="img-responsive center-block"></a>
                                        <div class="text-center">the Paraglider</div>
                                    </div>
                                </div>

                                <div class="item">
                                       <div class="col-md-1" >
                                        <a href="http://www.xcmag.com/"><img src="{{ url('/pictures/cross-country-magazine1.jpg') }}" target="_blank" class="img-responsive center-block"></a>
                                        <div class="text-center">Cross country magazine</div>
                                    </div>
                                </div>

                                <div class="item">
                                       <div class="col-md-1" >
                                        <a href="http://ziadbassil.blogspot.ro/"><img src="{{ url('/pictures/Ziad.jpg') }}" target="_blank" class="img-responsive center-block"></a>
                                        <div class="text-center">Dust of the universe</div>
                                    </div>
                                </div>

                                <div class="item">
                                       <div class="col-md-1" >
                                        <a href="https://flybubble.com/blog/"><img src="{{ url('/pictures/FLYBUBBLE.png') }}" target="_blank" class="img-responsive center-block"></a>
                                        <div class="text-center">Fly Bubble</div>
                                    </div>
                                </div>

                                <div class="item">
                                       <div class="col-md-1" >
                                        <a href="http://ojovolador.com/en/"><img src="{{ url('/pictures/logo_ojo_retina.png') }}" target="_blank" class="img-responsive center-block"></a>
                                        <div class="text-center">Ojovolador</div>
                                    </div>
                                </div>


                            </div>
                            <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                            <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                        </div>
                        <!--/carousel-inner-->
                    </div><!--/myCarousel-->
                <!--/well-->



    <style>
.carousel-inner.onebyone-carosel { margin: auto; width: 90%; }

.onebyone-carosel .active.left { left: -5%; }
.onebyone-carosel .active.right { left: 5%; }
.onebyone-carosel .next { left: 5%; }
.onebyone-carosel .prev { left: 5%; }
    </style>


<script>
$(document).ready(function () {
    $('#myCarousel').carousel({
        interval: 6000
    })
    $('.fdi-Carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));        

        if (next.next().next().next().next().length > 0) { // 1-5
            next.next().children(':first-child').clone().appendTo($(this));            
            next.next().next().children(':first-child').clone().appendTo($(this));            
            next.next().next().next().children(':first-child').clone().appendTo($(this));   
            next.next().next().next().next().children(':first-child').clone().appendTo($(this));   
        }
        else if (next.next().next().next().length > 0) { // 6
            next.next().children(':first-child').clone().appendTo($(this));            
            next.next().next().children(':first-child').clone().appendTo($(this));            
            next.next().next().next().children(':first-child').clone().appendTo($(this));   
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
     
        }
        else if (next.next().next().length > 0) { // 7
            next.next().children(':first-child').clone().appendTo($(this));
            next.next().next().children(':first-child').clone().appendTo($(this));
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            $(this).siblings(':first').next().children(':first-child').clone().appendTo($(this));
        }
        else if (next.next().length > 0) { //8
            next.next().children(':first-child').clone().appendTo($(this));
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            $(this).siblings(':first').next().children(':first-child').clone().appendTo($(this));
            $(this).siblings(':first').next().next().children(':first-child').clone().appendTo($(this));
        }  
        else { //done
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            $(this).siblings(':first').next().children(':first-child').clone().appendTo($(this));
            $(this).siblings(':first').next().next().children(':first-child').clone().appendTo($(this));    
            $(this).siblings(':first').next().next().next().children(':first-child').clone().appendTo($(this));    
            
        }
    });
});
</script>



    </body>
      @endsection
</html>
