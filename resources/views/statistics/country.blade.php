@extends('layouts.app')


@section('content')



<br><br><br>
<div class="container col-md-12">
  <div class="row">
    <div class="col-md-12 col-md-offset-0">
      <div class="panel panel-default">
        <div class="panel-heading">


COUNTRY - TESTE  - MODAL WINDOW

        </div>

        <div class="panel-body">

      <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>  -->

        <div id="toolbar">
         <button id="compareBtn" class="btn btn-info">Show only selected</button>
         <button id="showBtn" class="btn btn-info">Show all</button>
        </div>

         <table id="example"
         data-toolbar="#toolbar"
         data-toggle="table"
         data-search="true"
         data-icons-prefix="fa"
         data-icons="icons"
         data-filter-control="true" 
         data-sort-name="pilots"
         data-sort-order="desc"
         data-show-export="true"
         data-click-to-select="true"
         data-maintain-selected="true"
         data-show-toggle="true" 
         data-show-columns="true" 
         data-pagination="true" 
         data-filter-control="true"
         //class="table table-striped table-bordered table-hover" 
         cellspacing="0">
         
         <thead>

          <tr>
            <th data-field="1" data-field="state" data-sortable="true" data-checkbox="true">Select glider</th>
            <th data-field="Glider" data-filter-control="input" data-sortable="true">Glider</th>
            <th data-field="class" data-filter-control="select" data-sortable="true">Class</th>

            <th data-sortable="true" data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >A dist <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Average distance for all the flights made with the selected glider"></i></th>
            <th data-sortable="true" data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >A Speed <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Average speed for all the flights made with the selected glider"></i></th>
            <th data-sortable="true" data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >M dist <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Maximum distance for all the flights made with the selected glider"></i></th>
            <th data-sortable="true" data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >M point <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Maximum points for all the flights made with the selected glider"></i></th>
            <th data-sortable="true" data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >A15 <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Average distance for all the flights made with the selected glider over 15 km (XC limit set = 15km)"></i></th>
            <th data-sortable="true" data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >First <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Date of the first recorded flight made with the selected glider"></i></th>
            <th data-sortable="true" data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >Last <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Date of the latest recorded flight made with the selected glider"></i></th>
            <th data-field="pilots" data-sortable="true">Pilots</th>
            <th data-sortable="true">Flights</th>

          </tr>
        </thead>
        <tbody>
          @foreach ($total as $tot)
          <tr class="Collapse">
            <td class="bs-checkbox "><input data-index="{{ $tot->gliderID}}" name="btSelectItem" type="checkbox"></td>
            <td> <a href="{{ url('/gliders') }}/{{ $tot->gliderID}}">{{ $tot->Name_glider}}</a></td>
            <td> <b>{{ $tot->class }}</b></td>
            <td> {{ $tot->avgd }}</td>
            <td> {{ $tot->avgs }}</td>
            <td> {{ $tot->MAX }}</td>
            <td> {{ $tot->maxp }}</td>
            <td> {{ $tot->avg15 }}</td>
            <td> {{ $tot->FIRST }}</td>
            <td> {{ $tot->LAST }}</td>
            <td> {{ $tot->pilots }}</td>
            <td> {{ $tot->flights }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
        </div>
      </div>
    </div>
</div>




<div class="container">
  <!-- Content here -->
  <div class="alert alert-danger" role="alert">
    <strong>*TO DO!</strong>
    <li> After/before date: http://jsfiddle.net/djhvscf/e3nk137y/2414/</li>
    <li> Other BOOTSTRAP TABLE ideas: http://issues.wenzhixin.net.cn/bootstrap-table/</li>
    <li> Show selected ROWS: http://jsfiddle.net/Guruprasad_Rao/bypbqboe/64/ </li>
    <li> Sincronise Google Charts with top 10 pozitions from the table</li>
  </div>
</div>


<script>
//function for first dropdown button to select glider
  $(function(){
      // bind change event to select dropdown cu gliders list
      $('#glider-frm').on('submit', function (e) {
        e.preventDefault();
        location.href = $(this).data("url") + "/" + $("#glider").val();
      });
    });
  </script>

<script>
    window.icons = {
        refresh: 'fa-refresh',
        toggle: 'fa-toggle-on',
        columns: 'fa-th-list'
    };
</script>


<script type="text/javascript">
//compareshow all function
$(document).ready(function(){
    $("#compareBtn").click(function(){
        $(".Collapse").css("display", "none");
        $(".Collapse.selected").css("display", "table-row");
    });

    $("#showBtn").click(function(){
        $(".Collapse").css("display", "table-row");
    });
});
</script>


<!--
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
-->

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>



  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

@endsection
