@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-heading">
          <form  action=""  id="glider-frm" data-url="{{ url('/gliders') }}">
            <div class="form-group">
              <div class="col-sm-10">
                <select class="selectpicker form-control" data-live-search="true" id="glider" >
                  <option value="" disabled >Select glider</option>
                  @foreach ($aripi as $aripa){
                  <option value="{{ $aripa->id }}" >{{ $aripa->Name_glider }}</option>" }
                  @endforeach
                </select>
              </div>
              <input class="btn btn-primary" type="submit" value="Submit">
            </div>
          </form>
        </div>

        <div class="panel-body">

          <h1>General glider data : </h1>

           <table id="example"
                    data-show-toggle="true" 
         data-show-columns="true" 

         data-icons-prefix="fa"
         data-icons="icons"
         //data-toggle="table"
         data-sort-name="Points"
         data-sort-order="desc"
         data-show-export="true"
         //data-click-to-select="true"
         class="table table-striped"
         cellspacing="0">

            <thead>
              <tr>
                <th>Criteria</th>
                <th>Value</th>
              </tr>
            </thead>
            @foreach($data as $dat)
            <tr>
              <td>Glider name:</td>
              <td><b>{{ $Name_glider }}</b></td>
            </tr>
            <tr>
              <td>Glider class:</td>
              <td><b>{{ $class }}</b></td>
            </tr>
            <tr>
              <td>Glider top distance:</td>
              <td>{{ $dat->max }}</td>
            </tr>
            <tr>
              <td>Glider average distance:</td>
              <td>{{ $dat->avgd }}</td>
            </tr>
            <tr>
              <td>Glider average distance > 15 km:</td>
              <td>{{ $dat->avg15 }}</td>
            </tr>
            <tr>
              <td>Glider average speed:</td>
              <td>{{ $dat->avgs }}</td>
            </tr>
            <tr>
              <td>Glider max points:</td>
              <td>{{ $dat->maxp }}</td>
            </tr>
            <tr>
              <td>Pilots:</td>
              <td>{{ $dat->pilots }}</td>
            </tr>
            <tr>
              <td>Flights:</td>
              <td>{{ $dat->count }}</td>
            </tr>
            <tr>
              <td>First flight:</td>
              <td>{{ $dat->first }}</td>
            </tr>
            <tr>
              <td>Last flight:</td>
              <td>{{ $dat->last }}</td>
            </tr>
            <tr>
              <td>Average nr of gliders flied before this one:</td>
              <td>{{ $dat->first }}</td>
            </tr>
            <tr>
              <td>Average flights number before this glider:</td>
              <td>{{ $dat->last }}</td>
            </tr>
            <tr>
              <td>Contry with most pilots for this glider:</td>
              <td>{{ $dat->first }}</td>
            </tr>
            <tr>
              <td>Contry with most flighst for this glider:</td>
              <td>{{ $dat->last }}</td>
            </tr>
            <tr>
              <td>Contry with lingest flights for this glider:</td>
              <td>{{ $dat->first }}</td>
            </tr>
            <tr>
              <td>...:</td>
              <td>{{ $dat->last }}</td>
            </tr>
            @endforeach
          </table>
          <div id="corechart" style="width: 100%; height: 400px"></div>



          <h1>Geo data : </h1>
          <table class="table table-striped">
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/> -->
         <table id="Contry"
         //data-toggle="table"
         //data-search="true"
         data-sort-name="Pilots"
         data-icons-prefix="fa"
         data-icons="icons"
         data-sort-order="desc"
         data-show-export="true"
         //data-click-to-select="true"
         //data-show-toggle="true" 
         //data-show-columns="true" 
         data-pagination="true" 
         class="table table-striped"
         cellspacing="0">

            <thead>
              <tr>
                <th data-field="Country" data-sortable="true">Country</th>
                <th data-field="Adistance" data-sortable="true">Avg distance</th>
                <th data-field="Average" data-sortable="true">Avg speed</th>
                <th data-field="Max" data-sortable="true">Max distance</th>
                <th data-field="Flights" data-sortable="true">Flights</th>
                <th data-field="Pilots" data-sortable="true">Pilots</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($country as $tara)
              <tr>
                <td>{{ $tara->Country}}</td>
                <td>{{ $tara->avgdc }}</td>
                <td>{{ $tara->avgsc }}</td>
                <td>{{ $tara->max }}</td>
                <td>{{ $tara->count }}</td>
                <td>{{ $tara->pilots }}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 


          <div id="regions_div" style="width: 100%; height: 600px"></div>

          <h1>Chronological data : </h1>


 <table id="Chronological"
         //data-toggle="table"
         data-icons-prefix="fa"
         data-icons="icons"
         data-sort-name="Year"
         data-sort-order="desc"
         data-show-export="true"
         //data-click-to-select="true"
         //data-show-toggle="true" 
         //data-show-columns="true" 
         data-pagination="true" 
         class="table table-striped"
         cellspacing="0">

            <thead>
              <tr>
                <th data-field="Year" data-sortable="true">Year</th>
                <th data-field="Adistance" data-sortable="true">Avg distance</th>
                <th data-field="Average15" data-sortable="true">Avg distance > 15km</th>
                <th data-field="Averages" data-sortable="true">Avg speed</th>
                <th data-field="Max" data-sortable="true">Max distance</th>
                <th data-field="Flights" data-sortable="true">Flights</th>
                <th data-field="Pilots" data-sortable="true">Pilots</th>
                <th data-field="Delta" data-sortable="true">Delta</th>
                <th data-field="First" data-sortable="true">First flight</th>
                <th data-field="Last" data-sortable="true">Last flight</th>
              </tr>
            </thead>

            <tbody>
              @foreach ($chronological as $crono)
              <tr>
                <td>{{ $crono->year }}</td>
                <td>{{ $crono->avgdy }}</td>
                <td>{{ $crono->avg15y }}</td>
                <td>{{ $crono->avgsy }}</td>
                <td>{{ $crono->maxy }}</td>
                <td>{{ $crono->flights }}</td>
                <td>{{ $crono->pilots }}</td>
                <td></td>
                <td>{{ $crono->FIRST }}</td>
                <td>{{ $crono->LAST }}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 


          <div id="chrono" style="height:500px"></div>


<div class="row">
  <div class="col-md-1"></div>
  <div class="col-md-11" id="calendar" style="height: {{ $height }}px;"></div>
</div>

          


          <h1>Top 10 flights : </h1>


           <table id="Top10"
         //data-toggle="table"
         data-icons-prefix="fa"
         data-icons="icons"
         data-sort-name="Points"
         data-sort-order="desc"
         data-show-export="true"
         //data-click-to-select="true"
         //data-show-toggle="true" 
         //data-show-columns="true" 
         data-pagination="true" 
         class="table table-striped"
         cellspacing="0">

            <thead>
              <tr>
              <th data-field="Country" data-sortable="true">Country</th>
              <th data-field="Takeoff" data-sortable="true">Takeoff</th>
              <th data-field="Date" data-sortable="true">Date</th>
              <th data-field="Flight" data-sortable="true">Flight type</th>
              <th data-field="Distance" data-sortable="true">Distance</th>
              <th data-field="Points" data-sortable="true">Points</th>
              <th data-field="Speed" data-sortable="true">Speed</th>
              <th data-field="Link" data-sortable="true">Link</th>
              </tr>
            </thead>


            <tbody>
              @foreach ($top10 as $top)
              <tr>
                <td>{{ $top->Country }}</td>
                <td>{{ $top->takeoff }}</td>
                <td>{{ $top->date }}</td>
                <td>
    @if ($top->type == "FAI triangle")
      <img src="{{ url('/pictures/FAI_TRIANGLE.gif') }}" title="FAI TRIANGLE" />
    @elseif ($top->type == "free flight")  
      <img src="{{ url('/pictures/FREE_FLIGHT.gif') }}" title="FREE FLIGHT" />
    @elseif ($top->type == "flat triangle")  
      <img src="{{ url('/pictures/FREE_TRIANGLE.gif') }}" title="FLAT TRIANGLE" />
    @endif

                </td>
                <td>{{ $top->distance }}</td>
                <td>{{ $top->points }}</td>
                <td>{{ $top->speed }}</td>
                <td> <a href="{{ $top->link }}" target="_blank"><img src="{{ url('/pictures/show_flight.gif') }}" /></a> </td>
              </tr>
              @endforeach
            </tbody>
          </table> 

        </div>
      </div>
    </div>
  </div>
</div>

















<!-- SCRIPT PENTRU SELECT GLIDER DROPDOWN - FORMEAZA LINK-UL-->
<script>
  $(function(){
      // bind change event to select
      $('#glider-frm').on('submit', function (e) {
        e.preventDefault();
        location.href = $(this).data("url") + "/" + $("#glider").val();
      });
    });
  </script>


  <!--SCRIPTUL PENTRU CHARTUL  CRONOLOGICAL -->
  <script>

//          google.load('visualization', '1.0', { 'packages': ['corechart'] });
google.charts.load('current', {'packages':['corechart', 'calendar']});
//      google.charts.load("current", {packages:["calendar"]});




    //google.setOnLoadCallback(drawChartAtleta);
    //google.setOnLoadCallback(drawChartSexo);
    google.setOnLoadCallback(function () {
      drawChartAtleta();
      drawregions_div();
      drawcorechart();
      drawCalendar();

    });      

    //Grafico Atleta por esporte
    function drawChartAtleta() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Value');
      data.addColumn('number', 'Average distance < 15');
      data.addColumn('number', 'Max distance');
      data.addColumn('number', 'Flights');
      data.addColumn('number', 'Pilots');
      data.addRows([
        @foreach ($chronological as $crono)
        ['{{ $crono->year}}',{{ $crono->avg15y }}, {{ $crono->maxy }}, {{ $crono->flights }}, {{ $crono->pilots }} ],
        @endforeach
        ]);

      var options = {
        legend: { position: "bottom" },
        'title': 'Chronological evolution',
        trendlines: {
          0: {type: 'linear', lineWidth: 5, opacity: .3},
          1: {type: 'exponential', lineWidth: 10, opacity: .3}
        },
      };

        //This line was changed.
        var chart = new google.visualization.ColumnChart(document.getElementById('chrono'));
        chart.draw(data, options);
      }

    //Grafico por Sexo
    function drawregions_div() {
      var data = google.visualization.arrayToDataTable([
        ['Country', 'Flights', 'Average distance' ],
        @foreach ($country as $tara)
        ['{{ $tara->Country}}', {{ $tara->count }}, {{ $tara->avgdc }} ],
        @endforeach
        ]);

      var options = {
      };

      var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
      chart.draw(data, options);
    }






    function drawcorechart() {
      var data = google.visualization.arrayToDataTable([
        ['Distance', 'Nr.'],
        @foreach ($all as $al)
        [{{ $al->dist}}, {{ $al->count }} ],
        @endforeach
        ]);

      var options = {
        title: 'Distance distribution',
        hAxis: {title: 'Distance', minValue: 0, maxValue: 15},
        vAxis: {title: 'Count', minValue: 0, maxValue: 15},
        legend: 'none',
              trendlines: { 0: {} }    // Draw a trendline for data series 0.
            };

            var chart = new google.visualization.ScatterChart(document.getElementById('corechart'));

            chart.draw(data, options);
          }






//calendar
function drawCalendar() {

 var dataTable = new google.visualization.DataTable();
 dataTable.addColumn({ type: 'date', id: 'Date' });
 dataTable.addColumn({ type: 'number', id: 'Number' });
 dataTable.addRows([

  @foreach ($dates as $dat)        
  [ new Date({{ $dat->an}}, {{ $dat->luna }}, {{ $dat->zi }}), {{ $dat->count }} ],
  @endforeach
  ]);
 var chart = new google.visualization.Calendar(document.getElementById('calendar'));

 var options = {
   title: "Daily flight distribution",
 };

 chart.draw(dataTable, options);
};


$(document).ready(function() {
  $('#example').DataTable( {
    "order": [[ 3, "desc" ]],
    "ordering": true,
  } );
} );


</script>










<div class="container">
  <!-- Content here -->
  <div class="alert alert-danger" role="alert">
    <strong>*TO DO!</strong>
    <li> Complete data in first screen</li>
    <li> make google charts work from local</li>

  </div>
</div>









@endsection
