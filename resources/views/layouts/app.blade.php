  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>May Fly</title>

 
      <!-- Scripts -->
      <script>
          window.Laravel = <?php echo json_encode([
              'csrfToken' => csrf_token(),
          ]); ?>
      </script>





  
 <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="{{ url('/js/bootstrap.min.js') }}"></script> 


<!-- Latest compiled and minified CSS https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css-->
  <link href="{{ url('/css/bootstrap.min.css') }}" rel="stylesheet">






  <!-- JQUERY 3.1.1   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
  <script src="{{ url('/js/jquery.min.js') }}"></script>
 

<!-- Include Required Prerequisites -->
<script type="text/javascript" src="{{ url('js/moment.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap.css')}}" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="{{ url('js/daterangepicker.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ url('css/daterangepicker.css')}}" />






   <!--    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/> -->

  <!--<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/bootstrap-table.js"></script> -->
  <script src="{{ url('/js/bootstrap-table.js') }}"></script>
  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/filter-control/
  bootstrap-table-filter-control.js"></script> -->
  <script src="{{ url('/js/bootstrap-table-filter-control.js') }}"></script>


  <!--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/bootstrap-table.min.css"> -->
  <link rel="stylesheet" href="{{ url('/css/bootstrap-table.min.css') }}">

   


  <!-- select cu casuta de search -->
  <!-- Latest compiled and minified CSS https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css -->
  <link href="{{ url('/css/bootstrap-select.min.css') }}" rel="stylesheet">

  
  <!-- Latest compiled and minified JavaScript casuta cautat https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js-->
  <script src="{{ url('/js/bootstrap-select.min.js') }}"></script>

  <!-- CHARTS scripts https://www.gstatic.com/charts/loader.js    https://www.google.com/jsapi-->
  <script type="text/javascript" src="{{ url('/js/loader.js') }}"></script>
  <script type="text/javascript" src="{{ url('/js/jsapi') }}"></script>




<!-- Latest compiled JavaScript https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js
  <script src="{{ url('/js/bootstrap.js') }}"></script>-->




<!-- Styles -->
  <link href="{{ url('/css/app.css') }}" rel="stylesheet">




     <!-- Font awesome -->
      <link href="{{ url('/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type='text/css'>
  <!--   <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type='text/css'> -->




  <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #000000;">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}"><i class="fa fa-cloud-upload" aria-hidden="true"></i>
  May Fly</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="{{ url('/plan') }}"><i class="fa fa-map-signs" aria-hidden="true"></i>Plan a holliday</a></li>
          <li><a href="{{ url('/gliders') }}"><i class="fa fa-paper-plane" aria-hidden="true"></i>Gliders</a></li>
          <li><a href="{{ url('/charts/chart') }}"><i class="fa fa-area-chart" aria-hidden="true"></i>Charts</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-line-chart" aria-hidden="true"></i>Statistics <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Gliders</a></li>
              <li><a href="{{ url('statistics/country') }}">Country</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-form navbar-left">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i>
  Search</button>
        </form>
        <ul class="nav navbar-nav navbar-right">

  @if(Auth::user())
          <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i>
  Hi, {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</a></li>
          
          @endif
          <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>
  Contact</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-sign-in" aria-hidden="true"></i>Log in<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>
  Log in</a></li>
              <li><a href="{{ url('/register') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>
  Register</a></li>
                     </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>






  </head>
  <body>
<br><br><br><br>
@if (count($errors))
<div class="container ">
<div class="alert alert-warning">
  <ul>
    @foreach($errors->all() as $error)
      <li>{{$error }}</li>
    @endforeach
  </ul>
</div>
</div>
@endif

          @yield('content')
      </div>

      <!-- Scripts default js -->
      <script src="/js/app.js"></script>





  <div class="progress">
    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" height:"50px" style="width: 100%"></div>
  </div>



  <div class="footer" style="background-color: #227c25;">
  <div  class="container"><br>
    <!-- Content here -->
     <div class="alert alert-info" role="alert">
      <strong>*Disclaimer!</strong> If you encounter any error, please contact us.
     </div>
      <p >Contact information: <a href="mailto:someone@example.com">someone@example.com</a>.</p>
  </div>
  </div>

  <style>
  .footer {
      clear: both;
      position: relative;
      z-index: 10;
/*      height: 5em;*/
      margin-top: -3em;
  }
  </style>








  </body>
  </html>
