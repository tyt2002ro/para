@extends('layouts.app')


@section('content')


<br><br><br>
<div class="container col-md-12">
  <div class="row">
    <div class="col-md-12 col-md-offset-0">
      <div class="panel panel-default">
        <div class="panel-heading">
            Select glider to view details

          <form class="form-horizontal" action=""  id="glider-frm" data-url="{{ url('/gliders') }}">
            <div class="form-group">
              <div class="col-sm-10">
                <select class="selectpicker form-control" data-live-search="true" id="glider" >
                  <option value="" disabled selected>Select glider</option>
                  @foreach ($aripi as $aripa){
                  <option value="{{ $aripa->id }}">{{ $aripa->Name_glider }}</option>" }
                  @endforeach
                </select>


              </div>
              <input class="btn btn-primary" type="submit" value="Submit">
            </div>
          </form>
        </div>

        <div class="panel-body">

@foreach ($destination as $tara)
    <p>This country: {{ $tara->Country }} from this takeoff {{ $tara->takeoff }}, date: {{ $tara->date }} type: {{ $tara->type }}</p>
@endforeach


        </div>
    </div>
</div>



@endsection
