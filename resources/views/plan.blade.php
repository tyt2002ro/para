@extends('layouts.app')


@section('content')

<!--

      <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" media="all" href="css/daterangepicker.css" />
      <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -
      <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/daterangepicker.js"></script>


      <script type="text/javascript" src="js/moment.js"></script> 
-->

<div class="container ">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <div class="panel-heading">
          Find the perfect destination for your holidays according to your flying preferences.
        </div>

        <div class="panel-body">


          <form class="form-horizontal" method="post" action="{{ url('/trip') }}">
            {{ csrf_field ()}}
            <div class="form-group">
              <label class="control-label col-xs-2" for="inputEmail">Period:</label>
              <div class="col-xs-9">
                <input type="text" class="form-control" id="period" placeholder="Period" name="period" value=""  />
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-2" id="country">Flying type:</label>
              <div class="col-xs-9">
                <select name="type[]" title="All fly tipes" class="selectpicker" id="type" multiple data-width="100%">
                  <option value="free flight">Free flight</option>
                  <option value="FAI triangle">FAI triangle</option>
                  <option value="flat triangle">Flat triangle</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-2" id="zones">Continent:</label>
              <div class="col-xs-9">
                <select name="zones[]" class="selectpicker" title="All continents" id="zones" multiple data-width="100%">
                  <option value="Africa">Africa</option>
                  <option value="Asia">Asia</option>
                  <option value="North America">America North</option>
                  <option value="South America">America South</option>
                  <option value="Antarctica">Antarctica</option>
                  <option value="Australia">Australia</option>
                  <option value="Europe">Europe</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-2" id="country">Glider class:</label>
              <div class="col-xs-9">
                <select name="class[]" class="selectpicker" title="All gliders" id="class" multiple data-width="100%">

                  @foreach ($clase as $cla){
                  <option value="{{ $cla->class }}" >{{ $cla->CLAS }}</option>" }
                  @endforeach
                </select>
              </div>
            </div>
            <br>
            <div class="form-group">
              <div class="col-xs-offset-3 col-xs-9">
                <input type="submit" class="btn btn-primary" value="Recomand">
                <input type="reset" class="btn btn-default" value="Reset">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>






<div class="container">
  <!-- Content here -->
  <div class="alert alert-danger" role="alert">
    <strong>*TO DO!</strong>
    <li> Forms to autofill correlated to other forms. if...</li>
  </div>
</div>







<script type="text/javascript">
 // $(function() {



$('input[name="period"]').daterangepicker(
{
    locale: {
      format: 'YYYY-MM-DD'
    },

}, 
function(start, end, label) {
    ("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
});



  //});
</script>







@endsection


