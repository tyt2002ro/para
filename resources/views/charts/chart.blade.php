</!DOCTYPE html>
<html>
<head>
  <title></title>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.0/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/editable/bootstrap-table-editable.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/export/bootstrap-table-export.js"></script>
<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/filter-control/bootstrap-table-filter-control.js"></script>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.0/bootstrap-table.min.css">
<link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">


<style type="text/css">


.btn-warning{
    border-radius: 20px!important;
    border-color: #e67e22!important; 
    background-color: transparent!important;
    color: #e67e22!important;
    font-family: lato, sans-serif!important;
}
.btn-warning:hover,
.btn-warning:focus,
.btn-warning.focus,
.btn-warning:active,
.btn-warning.active,
.open > .dropdown-toggle.btn-warning {
  color: #fff !important;
  background-color: #ec971f!important;
  border-color: #d58512!important;
}

th{
   font-family: raleway, sans-serif!important;
}
body{
    font-family: lato, sans-serif!important;
}

.tooltip-inner{
    text-align:left!important;
    white-space:pre-wrap!important;
}

/*#table th:nth-child(3),*/
#table td:nth-child(3) {
    background-color: #f5f5f5;
}

.fixed-column {
    position: absolute;
    display: inline-block;
    width: auto;
    border-right: 1px solid #ddd;
}

@media(min-width:768px) {
    .table-responsive>.fixed-column {
        display: none;
    }
}
.table>thead:first-child>tr:first-child>th:first-child
{
    position: absolute;
  display: inline-block;
    background-color:red;
    height:100%;
}
.table> tbody > tr > td:first-child
{
    position: absolute;
  display: inline-block;
    background-color:red;
    height:100%;
    
}
.table>thead:first-child>tr:first-child>th:nth-child(2)
{
    padding-left:40px;
}
.table> tbody > tr > td:nth-child(2)
{
    padding-left:50px !important;
    
}






</style> 


</head>
<body>




<div class="container">


<div id="toolbar">
    <button id="compareBtn" class="btn btn-info">Compare</button>
    <button id="allBtn" class="btn btn-success">All</button>
    <button id="luxuryBtn" class="btn btn-warning">Luxury</button>
    <button id="sedanBtn" class="btn btn-warning">Sedan</button>
</div>
<div class="table-responsive">
<table id="table" 
       data-toolbar="#toolbar"
       data-search="true"
       data-show-toggle="true"
       data-show-columns="true"
       data-sort-name="1"
       data-sort-order="asc"
       data-click-to-select="true"
       data-classes="table table-hover table-no-bordered"
       data-maintain-selected="true"
       >
    <thead>
    <tr>
        <th data-field="state" data-checkbox="true" data-align="center" data-valign="middle"></th>
        <th data-field="0" data-sortable="true" data-align="left" data-valign="middle">Car Model</th>
        <th data-field="1" data-sortable="true"data-align="center" data-valign="middle" data-toggle="tooltip" data-placement="bottom" >Leasehackr Score <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="Leasehackr Score derives from blah blah blah"></i></th>
       <th data-field="6" data-sortable="true"data-align="center" data-valign="middle" >Residual (%)</th>
        <th data-field="11" data-sortable="true"data-align="center" data-valign="middle">Type</th>
        <th data-field="12" data-sortable="true"data-align="center" data-valign="middle">Size</th>
        <th data-field="2" data-sortable="true"data-align="center" data-valign="middle">Monthly Cost</th>
        <th data-field="3" data-sortable="true"data-align="center" data-valign="middle">Retail Price</th>
        <th data-field="4" data-sortable="true"data-align="center" data-valign="middle">Target Price</th>
        <th data-field="5" data-sortable="true"data-align="center" data-valign="middle">Bank Fees</th>

        <th data-field="7" data-sortable="true"data-align="center" data-valign="middle">Residual ($)</th>
        <th data-field="8" data-sortable="true"data-align="center" data-valign="middle">Money Factor</th>
        <th data-field="9" data-sortable="true"data-align="center" data-valign="middle">Months</th>
        <th data-field="10" data-sortable="true"data-align="center" data-valign="middle">Miles/yr</th>
        <th data-field="13" data-sortable="true"data-align="center" data-valign="middle">Valid Through</th>


    </tr>
    </thead>
</table>
</div>
<button id="button" class="btn btn-default">getData</button>
<i class="glyphicon glyphicon-pushpin" data-toggle="tooltip" data-placement="right" title="this is a tooltip glyphicon"></i>


</div>





<script type="text/javascript">


//Define tooltipExe()
function tooltipExe () {
    $('[data-toggle="tooltip"]').tooltip()
};

//Define pairData()
function pairData (obj){
    data = []; // Empty list
    for (var i = 0; i < obj.rows.length; i++) {
        row = obj.rows[i];
        data[i] = {}; // Empty dictionary object
        for (var j = 0; j < row.length; j++) {
            data[i][j.toString()] = row[j]; // Create a key and value pair.
        }
    }
};    

//Define fixNumbers(variable_data)
function fixNumbers (variable_data) {    
    for (var p=0; p<variable_data.length; p++){
        try {
            variable_data[p]["5"] = "$" + variable_data[p]["5"].toFixed(0);
        } catch(e) { }
    };
    for (var p=0; p<variable_data.length; p++){
        try {
            variable_data[p]["7"] = "$" + variable_data[p]["7"].toFixed(0);
        } catch(e) { }
    };
    for (var p=0; p<variable_data.length; p++){
        try {
            variable_data[p]["6"] = (100*variable_data[p]["6"]).toFixed(0) +"%";
        } catch(e) { }
    };
};  

//Define loadTable(variable_data)
var $table = $('#table');

function loadTable (variable_data) {
    $table.bootstrapTable('load', {
        data: variable_data,
    });
};

//Define global_data; Load initial table
var global_data;

$.getJSON("https://www.googleapis.com/fusiontables/v2/query?sql=SELECT%20*%20FROM%201Nzsjv6Fg4Jm1YzEa9mgUwHPRB8Fkpuurvlv9b8-v&key=AIzaSyARDzql1SCv34S7Vt9bNvxS_k3-pvZCzvw", function(obj){
    pairData(obj);
    global_data=data;
    fixNumbers (global_data);
    $('#table').bootstrapTable({
        data: global_data,
    });
    tooltipExe();
});

//Define luxury_data
var luxury_data;

$.getJSON("https://www.googleapis.com/fusiontables/v2/query?sql=SELECT%20*%20FROM%201Nzsjv6Fg4Jm1YzEa9mgUwHPRB8Fkpuurvlv9b8-v%20WHERE%20%27Type%27CONTAINS%27Luxury%27&key=AIzaSyARDzql1SCv34S7Vt9bNvxS_k3-pvZCzvw", function(obj) {
    pairData(obj);
    luxury_data = data;
    fixNumbers (luxury_data);
});

//define sedan_data
var sedan_data;

$.getJSON("https://www.googleapis.com/fusiontables/v2/query?sql=SELECT%20*%20FROM%201Nzsjv6Fg4Jm1YzEa9mgUwHPRB8Fkpuurvlv9b8-v%20WHERE%20%27Type%27CONTAINS%27Sedan%27&key=AIzaSyARDzql1SCv34S7Vt9bNvxS_k3-pvZCzvw", function(obj) {
    pairData(obj);
    sedan_data = data;
    fixNumbers (sedan_data);
    
});
    

//all button:
var $allBtn = $('#allBtn');
$allBtn.click(function () {
    loadTable(global_data);
    tooltipExe();
    });


//getData button:
     var $table = $('#table'),
        $button = $('#button');
    $(function () {
        $button.click(function () {
            alert(JSON.stringify($table.bootstrapTable('getData')));
        });
    });

//compare button:
var $compareBtn = $('#compareBtn');

$compareBtn.click(function () {
    $table.bootstrapTable('load',{
            data:$table.bootstrapTable('getSelections'),
        });
    tooltipExe();
    });

//Sedan Button:
var $sedanBtn = $('#sedanBtn')
$sedanBtn.click(function(){
    loadTable(sedan_data);   
    tooltipExe();       
});


//Luxury Button:
var $luxuryBtn = $('#luxuryBtn')
$luxuryBtn.click(function(){
    loadTable(luxury_data);   
    tooltipExe();       
});



</script>

</body>
</html>











