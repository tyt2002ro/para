<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('logout',  function () {
    Auth::logout();
        return view('welcome');
});


//see the gliders individual details
//Route::get('gliders/{id}', function ($id) {
//    $glider = App\Glider::find($id);
//    echo $glider->Name_glider;
//});

//see all the gliders
Route::get('gliders', 'GlidersController@index');
//see one glider
//Route::get('gliders/{glider}', 'GlidersController@show');


Route::get('gliders/{id}', 'GlidersController@show');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/plan', 'PlanController@index');

Route::get('charts/chart', 'ChartsController@chart');


Route::post('trip', 'PlanController@store');

Route::get('statistics/country', 'CountryController@index');