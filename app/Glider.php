<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Glider extends Model
{
    public function pilots(){
    	return $this->hasMany('App\Pilot');
    }


    public function gliders(){
    	return $this->hasMany('App\gliders', 'gliders.id', 'pilot.glider');
    }

}
