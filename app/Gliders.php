<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gliders extends Model
{
    public function pilots(){
    	return $this->belongsToMany('App\Glider');
    }
}
