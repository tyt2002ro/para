<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

$aripi = DB::select('SELECT DISTINCT gliders.gliderID as id, gliders.Name_glider FROM `gliders`');

//details about all the gliders
$total = DB::select("SELECT gliderID, Name_glider, case class when 1 then 'EN-A' when 12 then 'EN-B' when 2 then 'EN-C' when 23 then 'EN-D' when 3 then 'EN-CCC' when 122 then 'TANDEM A/B' when 222 then 'TANDEM C' when 7 then 'Rigid wing' when 5 then 'Hang Glider' when 9 then 'Speed glide/ride & Mountain' when 8 then 'Acro' when 6 then 'Paramotor' ELSE class end as class, round(avg(distance),2) AS avgd, round(avg(speed),2) AS avgs, max(distance) AS MAX, max(points) AS maxp, round(avg(CASE WHEN distance>15 THEN distance ELSE NULL END),2) AS avg15, min(date) AS FIRST, max(date) AS LAST, COUNT(DISTINCT Name) AS pilots, count(distinct pilot.id) AS flights FROM pilot join gliders where gliderid = glider_id GROUP BY glider_id");



                return view('statistics.country', compact('aripi', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
