<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


//details about all the gliders
        $clase = DB::select("SELECT class,
            (CASE CLASS
            WHEN 1 THEN 'EN-A'
            WHEN 12 THEN 'EN-B'
            WHEN 2 THEN 'EN-C'
            WHEN 23 THEN 'EN-D'
            WHEN 3 THEN 'EN-CCC'
            WHEN 122 THEN 'TANDEM A/B'
            WHEN 222 THEN 'TANDEM C'
            WHEN 7 THEN 'Rigid wing'
            WHEN 5 THEN 'Hang Glider'
            WHEN 9 THEN 'Speed glide/ride & Mountain'
            WHEN 8 THEN 'Acro'
            WHEN 6 THEN 'Paramotor'
            ELSE CLASS
            END) AS CLAS

            FROM gliders
            group by class
            order by CLAS");
//dd($clase);


        return view('plan', compact('clase'));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }






    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            //validates the fiels. if fails returns the same view with the error
            'period'=>'required'
            ]);



        //fac un string cu partea de query referitoare la flyight type;
        $typestring ="";
        echo $typestring.'  la inceput';
        if (!empty($_POST["type"])) {
            $arrayKeys = array_keys($_POST["type"]);
            $lastArrayKey = array_pop($arrayKeys);
            $typestring .= " and (";
            foreach($_POST["type"] as $k => $v) {
                if($k != $lastArrayKey) {
                    $typestring .= "type = '".$v."' or ";
                }else {
                    $typestring .= "type = '".$v."')";
                }
            }
        }
        echo $typestring;

        //fac un string cu partea de query referitoare la glider class;
        $classstring ="";
        if (!empty($_POST["class"])) {
            $arrayKeys = array_keys($_POST["class"]);
            $lastArrayKey = array_pop($arrayKeys);
            $classstring .= " and (";
            foreach($_POST["class"] as $k => $v) {
                if($k != $lastArrayKey) {
                    $classstring .= "class = '".$v."' or ";
                }else {
                    $classstring .= "class = '".$v."')";
                }
            }
        }
        echo $classstring;

DB::enableQueryLog();

        $destination = DB::select("
            SELECT * FROM `pilot` 
            where 
            SUBSTRING(date, 6) BETWEEN SUBSTRING('".substr($_POST["period"],0,10)."', 6) and SUBSTRING('".substr($_POST["period"],13,10)."', 6) 
            ".$typestring.$classstring."
            and (Country = 'IT')
            ORDER BY Tara ASC
            limit 30");


 dd(
            DB::getQueryLog()
        );

        echo '<br><br><br><br>foreach-uri:<br>';


        //logica pentru continent
        if (!empty($_POST["zones"])){
            if (in_array("Africa", $_POST["zones"])) {
                echo "a ales africa<br>";
            }
            if (in_array("Asia", $_POST["zones"])) {
                echo "a ales Asia<br>";
            }
            if (in_array("North America", $_POST["zones"])) {
                echo "a ales North America<br>";
            }
            if (in_array("South America", $_POST["zones"])) {
                echo "a ales South America<br>";
            }
            if (in_array("Antarctica", $_POST["zones"])) {
                echo "a ales South Antarctica<br>";
            }
            if (in_array("Australia", $_POST["zones"])) {
                echo "a ales Australia<br>";
            }
            if (in_array("Europe", $_POST["zones"])) {
                echo "a ales Europe<br>";
            }
        }



//        dd(request()->all());

        $aripi = DB::select('SELECT DISTINCT gliders.gliderID as id, gliders.Name_glider FROM `gliders`');

        return view('trip', compact('aripi', 'destination'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        dd($id);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}





/*
Continent   Sub-Continent   Countries
002 - Africa    015 - Northern Africa   DZ, EG, EH, LY, MA, SD, SS, TN
011 - Western Africa    BF, BJ, CI, CV, GH, GM, GN, GW, LR, ML, MR, NE, NG, SH, SL, SN, TG
017 - Middle Africa     AO, CD, ZR, CF, CG, CM, GA, GQ, ST, TD
014 - Eastern Africa    BI, DJ, ER, ET, KE, KM, MG, MU, MW, MZ, RE, RW, SC, SO, TZ, UG, YT, ZM, ZW
018 - Southern Africa   BW, LS, NA, SZ, ZA
150 - Europe    154 - Northern Europe   GG, JE, AX, DK, EE, FI, FO, GB, IE, IM, IS, LT, LV, NO, SE, SJ
155 - Western Europe    AT, BE, CH, DE, DD, FR, FX, LI, LU, MC, NL
151 - Eastern Europe    BG, BY, CZ, HU, MD, PL, RO, RU, SU, SK, UA
039 - Southern Europe   AD, AL, BA, ES, GI, GR, HR, IT, ME, MK, MT, CS, RS, PT, SI, SM, VA, YU
019 - Americas  021 - Northern America  BM, CA, GL, PM, US
029 - Caribbean     AG, AI, AN, AW, BB, BL, BS, CU, DM, DO, GD, GP, HT, JM, KN, KY, LC, MF, MQ, MS, PR, TC, TT, VC, VG, VI
013 - Central America   BZ, CR, GT, HN, MX, NI, PA, SV
005 - South America     AR, BO, BR, CL, CO, EC, FK, GF, GY, PE, PY, SR, UY, VE
142 - Asia  143 - Central Asia  TM, TJ, KG, KZ, UZ
030 - Eastern Asia  CN, HK, JP, KP, KR, MN, MO, TW
034 - Southern Asia     AF, BD, BT, IN, IR, LK, MV, NP, PK
035 - South-Eastern Asia    BN, ID, KH, LA, MM, BU, MY, PH, SG, TH, TL, TP, VN
145 - Western Asia  AE, AM, AZ, BH, CY, GE, IL, IQ, JO, KW, LB, OM, PS, QA, SA, NT, SY, TR, YE, YD
009 - Oceania   053 - Australia and New Zealand     AU, NF, NZ
054 - Melanesia     FJ, NC, PG, SB, VU
057 - Micronesia    FM, GU, KI, MH, MP, NR, PW
061 - Polynesia     AS, CK, NU, PF, PN, TK, TO, TV, WF, WS
*/