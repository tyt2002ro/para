<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Glider;

class GlidersController extends Controller
{
    // show all the glider in a table

  public function index(){

    $aripi = DB::select('SELECT DISTINCT gliders.gliderID as id, gliders.Name_glider FROM `gliders`');

//details about all the gliders
    $total = DB::select("SELECT gliderID, Name_glider, class, avgd, avgs, MAX, maxp, avg15, FIRST, LAST,  pilots, flights FROM rezumat_zboruri");

//insert into rezumat_zboruri (gliderID, Name_glider, class, avgd, avgs, MAX, maxp, avg15, FIRST, LAST, pilots, flights) SELECT gliderID, Name_glider, case class when 1 then 'EN-A' when 12 then 'EN-B' when 2 then 'EN-C' when 23 then 'EN-D' when 3 then 'EN-CCC' when 122 then 'TANDEM A/B' when 222 then 'TANDEM C' when 7 then 'Rigid wing' when 5 then 'Hang Glider' when 9 then 'Speed & Mountain' when 8 then 'Acro' when 6 then 'Paramotor' ELSE class end as class, round(avg(distance),2) AS avgd, round(avg(speed),2) AS avgs, max(distance) AS MAX, max(points) AS maxp, round(avg(CASE WHEN distance>15 THEN distance ELSE NULL END),2) AS avg15, min(date) AS FIRST, max(date) AS LAST, COUNT(DISTINCT Name) AS pilots, count(distinct pilot.id) AS flights FROM pilot join gliders where gliderid = glider_id GROUP BY glider_id


    return view('gliders', compact('aripi', 'total'));


  }







  public function show($id){

    $aripi = DB::select('SELECT DISTINCT gliders.gliderID as id, gliders.Name_glider FROM `gliders`');

    $classa = DB::select( DB::raw("SELECT case class when 1 then 'EN-A' when 12 then 'EN-B' when 2 then 'EN-C' when 23 then 'EN-D' when 3 then 'EN-CCC' when 122 then 'TANDEM A/B' when 222 then 'TANDEM C' when 7 then 'Rigid wing' when 5 then 'Hang Glider' when 9 then 'Speed glide/ride & Mountain' when 8 then 'Acro' when 6 then 'Paramotor' ELSE class end as class FROM `gliders` where `gliderID` = :id"), array(
     'id' => $id,
     ));
    $class = $classa[0]->class;

    $data = DB::select( DB::raw("select round(avg(pilot.distance),2) as avgd, round(avg(pilot.speed),2) as avgs, max(pilot.distance) as max, max(pilot.points) as maxp, count(pilot.distance) as count, round(avg(case when pilot.distance>'15' then pilot.distance else null end),2) as avg15, min(pilot.date) as first, max(pilot.date) as last, COUNT(DISTINCT pilot.Name) as pilots from pilot where glider_ID = :id group by glider_id "), array(
     'id' => $id,
     ));

    $all = DB::select( DB::raw("select pilot.distance as dist, count(pilot.distance) as count from pilot where glider_id = :id group by pilot.distance  "), array(
     'id' => $id,
     ));

    $dates = DB::select( DB::raw("select SUBSTRING(date,1,4) as an, SUBSTRING(date,6,2) as luna, SUBSTRING(date,9,2) as zi, date, count(date) as count 
      from pilot 
      where glider_id = :id
      group by date"), array(
      'id' => $id,
      ));


    $top10=DB::select( DB::raw("SELECT Country, takeoff, date, type, distance, points, speed, link FROM `pilot` where glider_id = :id order by points DESC limit 10 "), array(
     'id' => $id,
     ));


    $Name_glide=DB::select( DB::raw("SELECT Name_glider FROM `gliders` where gliderID = :id"), array(
     'id' => $id,
     ));

    $Name_glider=$Name_glide[0]->Name_glider;


    $country = DB::select( DB::raw("select Country, ROUND(avg(pilot.distance),2) as avgdc, ROUND(avg(pilot.speed),2) as avgsc, max(pilot.distance) as max, count(Country) as count, count(distinct Name) as pilots
      from pilot 
      where glider_id = :id
      group by Country 
      order by pilots desc"), array(
      'id' => $id,
      ));

    $chronological = DB::select( DB::raw("SELECT 
     substring(pilot.date,1, 4) as year,
     round(avg(pilot.distance),2) AS avgdy,
     round(avg(pilot.speed),2) AS avgsy,
     max(pilot.distance) AS maxy,
     count(pilot.distance) AS flights,
     round(avg(CASE
     WHEN pilot.distance>'15' THEN pilot.distance
     ELSE NULL
     END),2) AS avg15y,
     min(pilot.date) AS FIRST,
     max(pilot.date) AS LAST,
     COUNT(DISTINCT pilot.Name) AS pilots
     FROM pilot
     WHERE glider_id = :id
     GROUP BY substring(pilot.date,1, 4)"), array(
     'id' => $id,
     ));






    $countchronological = DB::select( DB::raw("SELECT min(substring(pilot.date,1, 4)) AS FIRST, max(substring(pilot.date,1, 4)) AS LAST FROM pilot WHERE glider_id = :id "), array('id' =>$id,));

    $height = 155*($countchronological[0]->LAST - $countchronological[0]->FIRST + 1);


 //$aripa = DB::select('select glider from pilot where glider ='.$glide)->first();
    return view('glider', compact('data', 'id', 'distance_avg', 'aripi', 'Name_glider', 'country', 'chronological', 'all', 'dates', 'height', 'class', 'top10')) ;

  }
}












$aripi = DB::select('SELECT DISTINCT gliders.id, gliders.Name_glider FROM `gliders` where id = 1340 or id = 4611 or id = 3740');

foreach ($aripi as $aripa) {

}
/*

    update para.pilot left join para.gliders on pilot.glider = gliders.Name_user_set set pilot.glider_id = gliders.gliderID 

    public function show($id){

$aripi = DB::select('SELECT DISTINCT gliders.id, gliders.Name_glider FROM `gliders` where id = 1340 or id = 4611 or id = 3740');


$data = DB::select( DB::raw("select round(avg(pilot.distance),2) as avgd, round(avg(pilot.speed),2) as avgs, max(pilot.distance) as max, max(pilot.points) as maxp, count(pilot.distance) as count, round(avg(case when pilot.distance>'15' then pilot.distance else null end),2) as avg15, gliders.Name_glider, min(pilot.date) as first, max(pilot.date) as last, COUNT(DISTINCT pilot.Name) as pilots
	from pilot 
	left join gliders on gliders.id=pilot.glider_id 
	where gliders.gliderID = 1
	group by gliders.Name_glider "), array(
   'id' => $id,
 ));

$all = DB::select( DB::raw("select pilot.distance as dist, count(pilot.distance) as count from pilot left join gliders on gliders.id=pilot.glider_id where gliders.gliderID = :id group by pilot.distance "), array(
   'id' => $id,
 ));

$dates = DB::select( DB::raw("select SUBSTRING(pilot.date,1,4) as an, SUBSTRING(pilot.date,6,2) as luna, SUBSTRING(pilot.date,9,2) as zi, pilot.date, count(pilot.date) as count from pilot left join gliders on gliders.id=pilot.glider_id where gliders.gliderID = :id group by pilot.date "), array(
   'id' => $id,
 ));




$Name_glider=$data[0]->Name_glider;


$country = DB::select( DB::raw("select Country, ROUND(avg(pilot.distance),2) as avgdc, ROUND(avg(pilot.speed),2) as avgsc, max(pilot.distance) as max, count(Country) as count, count(distinct Name) as pilots
    from pilot 
    left join gliders on gliders.id=pilot.glider_id 
    where gliders.gliderID = :id 
    group by Country 
    order by pilots desc"), array(
   'id' => $id,
 ));

$chronological = DB::select( DB::raw("SELECT 
       substring(pilot.date,1, 4) as year,
       round(avg(pilot.distance),2) AS avgdy,
       round(avg(pilot.speed),2) AS avgsy,
       max(pilot.distance) AS maxy,
       count(pilot.distance) AS flights,
       round(avg(CASE
                     WHEN pilot.distance>'15' THEN pilot.distance
                     ELSE NULL
                 END),2) AS avg15y,
       min(pilot.date) AS FIRST,
       max(pilot.date) AS LAST,
       COUNT(DISTINCT pilot.Name) AS pilots
FROM pilot
LEFT JOIN gliders ON gliders.id=pilot.glider_id
WHERE gliders.gliderID = :id 
GROUP BY substring(pilot.date,1, 4)"), array(
   'id' => $id,
 ));



 //$aripa = DB::select('select glider from pilot where glider ='.$glide)->first();
 return view('glider', compact('data', 'id', 'distance_avg', 'aripi', 'Name_glider', 'country', 'chronological', 'all', 'dates')) ;

    }
}

*/