<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charts extends Model
{
    protected $table = 'charts';
    public $fillable = ['country','users'];
    
}
